!------------------------------------------------------------------------------!
! Numerical Weather Prediction - Programming Excercise
!------------------------------------------------------------------------------!
! Tolles Programm fuer das Praktikum

! Description
!  Dieses Programm berechnet eine Vorhersage von Geopotential, Vorticity und Wind fuer eine gewuenschte
!  Anzahl an Stunden bei vorgegebenen Anfangswerten.

!------------------------------------------------------------------------------!
! Author: Tobias Gronemeier, modified by Patrick Heuss
! Date  : 2018-02-14, last change 2019-03-08
!------------------------------------------------------------------------------!
! NOTE: Code compilation
!  When compiling the code, NetCDF libraries must be included/linked like so
!    gfortran nwp_program_template.f90 -I /muksoft/packages/netcdf/4_gnu/include -L /muksoft/packages/netcdf/4_gnu/lib -lnetcdff
!  2018-02-07, TG

PROGRAM nwp_program

   USE netcdf

   IMPLICIT NONE


   !- Deklariere Variablen
   CHARACTER(LEN=100) :: file_in = '/home/heuss/NWP/nwp_template/src/era_int_eu_20171024_20171026.nc'
   CHARACTER(LEN=100) :: file_out = "nwp_output_1"   !< Name der Ausgabedatei (Namelist Parametereter)
   
   INTEGER :: i               !< Laufindex
   INTEGER :: j               !< Laufindex
   INTEGER :: k               !< Laufindex
   INTEGER :: t               !< Laufindex
   INTEGER :: k_max = 10000   !< Maximaler Iterationsschritt der SOR-Methode (Namelist Parameter)
   INTEGER :: nx = 399        !< Gitterpunkte in x-Richtung (Namelist Parameter)
   INTEGER :: ny = 200        !< Gitterpunkte in y-Richtung (Namelist Parameter)
   INTEGER :: nt              !< Zeitschritte
   INTEGER :: nt_ref = 8
   INTEGER :: tt

   REAL :: beta               !< beta-Parameter
   REAL :: delta = 30000.0    !< Gitterbreite/-laenge (Namelist Parameter)
   REAL :: lx                 !< Wellenlaenge x-Richtung
   REAL :: ly                 !< Wellenlaenge y-Richtung
   REAL :: dt                 !< Zeitschrittgroeße
   REAL :: dt_u               !< um Zeitschritt festzulegen
   REAL :: dt_v               !< um Zeitschritt festzulegen
   REAL :: dt_do              !< Zeitschritt der Ausgabe
   REAL :: f_0                !< Coriolisparameter bei y=0
   REAL :: latitude           !< Breitengrad (Namelist Parameter)
   REAL :: omega              !< Relaxationsfaktor der SOR-Methode
   REAL :: t_end              !< Simulationszeit (s)
   REAL :: t_sim              !< simulierte Zeit (s)
   REAL :: ts                 !< simulierte Zeit (h)
   REAL :: time_end = 24.0    !< Simulationszeit (h, Namelist Parameter)
   REAL :: time_dt_do = 24.0  !< Zeitintervall der Ausgabe (h, Namelist Parameter)
   REAL :: u_bg               !< Grundstrom
   REAL :: u_max              !< Maximale Windgeschwindigkeit
   REAL :: c                  !< Verlagerungsgeschwindigkeit
   REAL, DIMENSION(2) :: phi_min_a              !< Minimum des Geopotentials am Anfang
   REAL, DIMENSION(2) :: phi_min_e              !< Minimum des Geopotentials am Ende
   REAL, DIMENSION(:), ALLOCATABLE :: phi_a     !< Amplitude des Geopotentials

   REAL, DIMENSION(:), ALLOCATABLE :: f         !< Coriolisparameter entlang y

   REAL, DIMENSION(:,:), ALLOCATABLE :: phi     !< Geopotential
   REAL, DIMENSION(:,:), ALLOCATABLE :: u       !< Windgeschwindigkeit entlang x
   REAL, DIMENSION(:,:), ALLOCATABLE :: v       !< Windgeschwindigkeit entlang y
   REAL, DIMENSION(:,:), ALLOCATABLE :: zeta    !< Vorticity zur Zeit t
   REAL, DIMENSION(:,:), ALLOCATABLE :: zeta_m  !< Vorticity zur Zeit t-1
   REAL, DIMENSION(:,:), ALLOCATABLE :: zeta_p  !< Vorticity zur Zeit t+1
   REAL, DIMENSION(:,:), ALLOCATABLE :: zeta_r

   REAL, DIMENSION(:,:,:), ALLOCATABLE :: phi_ref  !< Referenz-Geopotential

   REAL, PARAMETER :: g = 9.81      !< Schwerebeschleunigung
   REAL, PARAMETER :: pi = 3.14159  !< Pi
   
   INTEGER :: ioerr
   INTEGER :: Faelle                            !< Cases in Uebung 1
   
   REAL :: summe
   
   !- Deklariere Variablen fuer Zeitmessung
   INTEGER :: count, count_rate
   REAL :: diff_SOR
   REAL :: diff_Zeitschritt
   REAL :: diff_Wind
   REAL :: diff_Ausgabe
   REAL :: diff_gesamt
   REAL :: SOR_gesamt = 0
   REAL :: Zeitschritt_gesamt = 0
   REAL :: Wind_gesamt = 0

   !- Definiere und lese init-namelist aus
  namelist / INIT / delta, file_out, k_max, latitude, nx, ny, dt, time_dt_do, &
                    time_end, Faelle, u_bg
                    
      CALL system_clock (count, count_rate)  
        diff_gesamt = REAL(count)/REAL(count_rate)
                    
      !- Start
   WRITE(*, '(A//A)') ' --- NWP program, V0.0.1 ---',  &
                      ' --- start forecast...'
                    
   OPEN(1, FILE='./nwp_prog.init', STATUS='OLD', FORM='FORMATTED', IOSTAT=ioerr)

   READ(1, NML=INIT, IOSTAT=ioerr)
   CLOSE(1)


   !- Initialisiere Variablen
   ! Konvertiere Stunden in Sekunden
   t_end = time_end * 3600.0 ![t_end]=s

   ! Gebe maximale Windgeschwindigkeit fuer u vor
   u_max = 10.0      ![umax]=m/s

   ! Allokiere Variablen
   ALLOCATE(phi(-1:nx+1,-1:ny+1))
   ALLOCATE(zeta(-1:nx+1,-1:ny+1))
   ALLOCATE(f(0:ny))
   ALLOCATE(u(-1:nx+1,-1:ny+1))
   ALLOCATE(v(-1:nx+1,-1:ny+1))
   ALLOCATE(phi_ref(-1:nx+1,-1:ny+1,0:nt_ref))
   ALLOCATE(phi_a(0:ny))
   ALLOCATE(zeta_p(-1:nx+1,-1:ny+1))
   ALLOCATE(zeta_m(-1:nx+1,-1:ny+1))
   ALLOCATE(zeta_r(0:nx,0:ny))
   
   !- beta
    !beta=2*pi/86164.*COS(latitude)
    beta=1.98E-11
    !beta=1.62E-11                     !< beta @45° (Uebung 2)
    !beta=0                            !< nur Uebung 2 Aufgabe 1
    
   !- phi
    phi(:,:)=0.0
    phi_ref(:,:,:)=0.0
   
   !- zeta
    zeta(:,:)=0.0
    zeta_p(:,:)=0.0
    zeta_m(:,:)=0.0
    
   
   !- Coriolisparameter
    f_0 = 7.2E-5
    
    DO j=0,ny
        f(j)=f_0+beta*REAL(j*delta)
    END DO

   
!-----------------------------------------------------------------------------
    !- Definiere verschiedene Cases (nur Uebungen 1 und 2)
!    SELECT CASE (Faelle)
!    
!     CASE (1)
!       
!       phi=0.0
!       zeta=0.0
!       f(:)=0.0001
!       phi(:,0)=0.0
!       phi(:,ny)=50.0   
!       
!       DO j=0,ny
!         phi_ref(:,j)=50.0/ny*j
!       END DO
!       
!     CASE (2)
!     
!       f(:)=0.0001
!       phi=0.0
!       zeta=0.0001
!       
!       
!       DO j=0,ny
!         phi_ref(:,j)=(1.0/2.0)*0.00000001*(delta*j)**2 &
!                     -(1.0/2.0)*0.00000001*ny*delta*j*delta
!       END DO
!     
!     CASE (3)
!     
!       phi=0.0
!       phi(:,0)=0.0
!       phi(:,ny)=0.0
!       !f(:)=0.0001
!       lx=nx*delta
!       ly=2*ny*delta
!       
!       
!       DO j=1,ny-1
!         DO i=0,nx
!             phi_a(j)=ABS(u_max)*f(j)*ly/(2.0*pi)
!             
!             phi_ref(i,j)=phi_a(j)*sin(2.0*pi/(lx)*i*delta) &
!                          *sin(2.0*pi/(ly)*j*delta)
!             
!             zeta(i,j)=-1.0/f(j)*(4*pi**2/((lx)**2)+4*pi**2/((ly)**2)) &
!                       *phi_ref(i,j)
!         END DO
!       END DO
!       
!    END SELECT
!-----------------------------------------------------------------------------
!-----------------------------------------------------------------------------   
   !- Abruf vom Geopotential und vom Wind sowie vom Zeitschritt (hier nur fuer Uebungen 1 und 2)
    !CALL get_geopot(k_max)
  
    !CALL get_wind

    !dt=timestep()
!-----------------------------------------------------------------------------

    PRINT *,'dt =', dt
    PRINT *,'dt_u =', dt_u
    PRINT *,'dt_v =', dt_v
   
   !- Lese Eingabedaten
    CALL read_input_data(file_in)
   
   !- Oeffne Ausgabedatei
    CALL data_output('open', file_out)   
   
   
    phi=phi_ref(:,:,0)

    
    DO j=0,ny
        DO i=0,nx
            zeta(i,j)   =1.0/f(j)&
                        *((phi(i+1,j)-2.0*phi(i,j)+phi(i-1,j))/(delta**2)&
                        +(phi(i,j+1)-2.0*phi(i,j)+phi(i,j-1))/(delta**2))
        END DO
    END DO
      
   CALL get_wind
   
   CALL data_output('write', file_out)

   !- Starte Vorhersage
    t_sim = 0.0
    nt = 1
    
    PRINT *,'nt =', nt
    DO  WHILE (t_sim < t_end)
    
        ts=t_sim/3600
        tt = INT(ts/6)
        dt=(timestep())
        PRINT *,'dt', dt
    
       CALL randwerte 
 
       CALL get_vorticity

       CALL get_geopot(k_max)
       
! !------------------------------------------------------
!     !- Finde Gebiet von phi_min heraus (nur Uebung 2)
!        SELECT CASE (t)
!         
!         CASE(1)
!             phi_min_a=minloc(phi)
!         
!         CASE(127)
!             phi_min_e=minloc(phi)          
!         
!        END SELECT
! !------------------------------------------------------  
      
       CALL get_wind
       
        t_sim=t_sim+dt
        nt=nt+1
        
       CALL data_output('write', file_out)
       
       WRITE(*,*) 'Schritt', t_sim, 'von', t_end
       
    
    
 
    ENDDO
    
    
   !- Berechne Verlagerungsgeschwindigkeit (nur Uebung 2) 
!     c=(phi_min_e(1)*delta-phi_min_a(1)*delta)/t_end     
!     WRITE(*,*) 'c =', c


   !- Finalisiere Ausgabe
   CALL data_output('close', file_out)

   CALL system_clock (count, count_rate)           
        diff_gesamt = REAL(count)/REAL(count_rate) - diff_gesamt

    ! Ausgabe der Berechnungszeiten
    Print *, 'Berechnungszeit SOR-Verfahren:', SOR_gesamt, 's', SOR_gesamt/diff_gesamt*100, '%'
    Print *, 'Berechnungszeit Zeitschrittverfahren:', Zeitschritt_gesamt, 's', Zeitschritt_gesamt/diff_gesamt*100, '%'
    Print *, 'Berechnungszeit Windgeschwindigkeiten:', Wind_gesamt, 's', Wind_gesamt/diff_gesamt*100, '%'
    Print *, 'Berechnungszeit Datenausgabe', diff_Ausgabe, 's', diff_Ausgabe/diff_gesamt*100, '%'
    Print *, 'Gesamtberechnungszeit', diff_gesamt, 's'
    
  
   !- Ende
   WRITE(*, '(A)') ' --- Finished! :-) ---'

 CONTAINS
!- Funktionen und Subroutinen

   REAL FUNCTION timestep()
   ! Berechne Zeitschritt
            dt_u=delta/MAXVAL(u)
            dt_v=delta/MAXVAL(v)
            timestep=0.5*MIN(dt_u,dt_v)
   END FUNCTION timestep

   !----------------------------------------------------------------------------

   
   SUBROUTINE get_geopot(max_it)
   ! Berechne Geopotential mit der SOR-Methode

      INTEGER, INTENT (IN) :: max_it  !< maximum iteration count

      REAL, DIMENSION(-1:nx+1,0:ny) :: phialt
      REAL :: epsi
      REAL :: epsilon_grenz
      
      omega=2.0-(2.0*pi)/SQRT(2.0)*SQRT(1.0/(nx+1.0)**2+1.0 / (ny+1.0)**2 )
      
      epsilon_grenz=0.00001
           
      
      
    CALL system_clock (count, count_rate)          
        diff_SOR = REAL(count)/REAL(count_rate)
      
        DO k=1,max_it
            DO j=0,ny
                DO i= 0,nx
                    phi(i,j)=phi(i,j) &
                    *(1-omega) &
                    +omega/4*(phi(i+1,j)+phi(i-1,j)+phi(i,j+1)+phi(i,j-1) &
                    -f(j)*delta**2*zeta(i,j))
                END DO
            END DO
            epsi=MAXVAL(ABS(phi-phialt))
            phialt=phi
            !phi(-1,:)=phi(nx,:)        !Randwerte fuer Uebungen 1 und 2
            !phi(nx+1,:)=phi(0,:) 
            
            IF (epsi .LE. epsilon_grenz) THEN
                !WRITE (*,*) k, 'Iterationsschritte'
                EXIT
            END IF
        END DO
        
    CALL system_clock (count, count_rate)           
        diff_SOR = REAL(count)/REAL(count_rate) - diff_SOR
        SOR_gesamt = SOR_gesamt+diff_SOR
            
   END SUBROUTINE get_geopot

   !----------------------------------------------------------------------------

   SUBROUTINE get_vorticity
   ! Berechne Vorticity ueber die Prognostische Gleichung
      
      CALL system_clock (count, count_rate)          
        diff_Zeitschritt = REAL(count)/REAL(count_rate)
      
      SELECT CASE (nt)
      
        CASE (1)                    !Euler-Verfahren
            DO j=1,ny-1
                DO i=1,nx-1
                    zeta_p(i,j) &
                    =zeta(i,j) &
                    +dt*(-u(i,j)*(zeta(i+1,j)-zeta(i-1,j))/(2.0*delta) &
                    -v(i,j)*(zeta(i,j+1)-zeta(i,j-1))/(2.0*delta)-v(i,j)*beta)
                END DO
            END DO
            
     
            
        CASE (2:)                   !Leapfrog-Verfahren
            DO j=1,ny-1
                DO i=1,nx-1
                    zeta_p(i,j) &
                    =zeta_m(i,j) &
                    +2.0*dt*(-u(i,j)*(zeta(i+1,j)-zeta(i-1,j))/(2.0*delta) &
                    -v(i,j)*(zeta(i,j+1)-zeta(i,j-1))/(2*delta)-v(i,j)*beta)
                END DO
            END DO


            
      END SELECT
      
      
    ! Randwerte fuer Uebungen 1 und 2      
!       zeta(-1,:)=zeta(nx,:)
!       zeta(nx+1,:)=zeta(0,:)
!       zeta_p(-1,:)=zeta_p(nx,:)
!       zeta_p(nx+1,:)=zeta_p(0,:)
!       zeta_m(-1,:)=zeta_m(nx,:)
!       zeta_m(nx+1,:)=zeta_m(0,:)
      
        zeta_m=zeta
        zeta=zeta_p
      
      CALL system_clock (count, count_rate)           
        diff_Zeitschritt = REAL(count)/REAL(count_rate) - diff_Zeitschritt
        Zeitschritt_gesamt=Zeitschritt_gesamt+diff_Zeitschritt
            
   END SUBROUTINE get_vorticity

   !----------------------------------------------------------------------------

   SUBROUTINE get_wind
   ! Berechne Windgeschwindigkeiten mit der geostrophischen Windrelation
     
     CALL system_clock (count, count_rate)         
        diff_Wind = REAL(count)/REAL(count_rate)
     
        DO j=0,ny
            DO i=0,nx
                u(i,j)=-1.0/f(j)*(phi(i,j+1)-phi(i,j-1))/(2.0*delta)
                v(i,j)=1.0/f(j)*(phi(i+1,j)-phi(i-1,j))/(2.0*delta)
            END DO
        END DO

    ! Randwerte fuer Uebungen 1 und 2
!         u(-1,:)=u(nx,:)       
!         u(nx+1,:)=u(0,:)
!         v(-1,:)=v(nx,:)
!         v(nx+1,:)=v(0,:)
        
      CALL system_clock (count, count_rate)          
        diff_Wind = REAL(count)/REAL(count_rate) - diff_Wind
        Wind_gesamt=Wind_gesamt+diff_Wind
        
   END SUBROUTINE get_wind

   !----------------------------------------------------------------------------
   
   SUBROUTINE randwerte
   ! Berechne Randwerte fuer Vorhersage (Uebung 3) 
   
        DO j=-1,ny+1
            DO i=-1,nx+1
      
            phi(0,j)=phi_ref(0,j,tt) &
                +(phi_ref(0,j,tt+1)-phi_ref(0,j,tt))/((tt+6)-tt)*(ts-tt)
            
            phi(-1,j)=phi_ref(-1,j,tt) &
                +(phi_ref(-1,j,tt+1)-phi_ref(-1,j,tt))/((tt+6)-tt)*(ts-tt)
            
            phi(1,j)=phi_ref(1,j,tt) &
                +(phi_ref(1,j,tt+1)-phi_ref(1,j,tt))/((tt+6)-tt)*(ts-tt)
            
            phi(nx,j)=phi_ref(nx,j,tt) &
                +(phi_ref(nx,j,tt+1)-phi_ref(nx,j,tt))/((tt+6)-tt)*(ts-tt)
            
            phi(nx+1,j)=phi_ref(nx+1,j,tt) &
                +(phi_ref(nx+1,j,tt+1)-phi_ref(nx+1,j,tt))/((tt+6)-tt)*(ts-tt)
            
            phi(nx-1,j)=phi_ref(nx-1,j,tt) &
                +(phi_ref(nx-1,j,tt+1)-phi_ref(nx-1,j,tt))/((tt+6)-tt)*(ts-tt)
            
            phi(i,0)=phi_ref(i,0,tt) &
                +(phi_ref(i,0,tt+1)-phi_ref(i,0,tt))/((tt+6)-tt)*(ts-tt)
            
            phi(i,-1)=phi_ref(i,-1,tt) &
                +(phi_ref(i,-1,tt+1)-phi_ref(i,-1,tt))/((tt+6)-tt)*(ts-tt)
            
            phi(i,1)=phi_ref(i,1,tt) &
                +(phi_ref(i,1,tt+1)-phi_ref(i,1,tt))/((tt+6)-tt)*(ts-tt)
            
            phi(i,ny)=phi_ref(i,ny,tt) &
                +(phi_ref(i,ny,tt+1)-phi_ref(i,ny,tt))/((tt+6)-tt)*(ts-tt)
            
            phi(i,ny-1)=phi_ref(i,ny-1,tt) &
                +(phi_ref(i,ny-1,tt+1)-phi_ref(i,ny-1,tt))/((tt+6)-tt)*(ts-tt)
                    
            phi(i,ny+1)=phi_ref(i,ny+1,tt) &
                +(phi_ref(i,ny+1,tt+1)-phi_ref(i,ny+1,tt))/((tt+6)-tt)*(ts-tt)
        
            END DO
        END DO
        
        DO j = 0,ny
            DO i = 0,nx
                zeta_r(i,j) &
                = (1/f(j)) &
                * ((phi(i+1,j) - 2.0 * phi(i,j) + phi(i-1,j))/delta**2 &
                + (phi(i,j+1) - 2.0 * phi(i,j) + phi(i,j-1))/delta**2)
            END DO
        END DO
   
        zeta(0:nx,ny) = zeta_r(:,ny)
        zeta(0:nx,0) = zeta_r(:,0)
        zeta(nx,0:ny) = zeta_r(nx,:)
        zeta(0,0:ny) = zeta_r(0,:)
   
   END SUBROUTINE randwerte
   
   !----------------------------------------------------------------------------

   SUBROUTINE read_input_data(input)
   ! Read input from mesoscale model
   
   CHARACTER(LEN=*), INTENT(IN) :: input  !< name of input file

      INTEGER, SAVE :: id_file         !< ID of NetCDF file
      INTEGER, SAVE :: id_var_in       !< ID of input array
      INTEGER, SAVE :: nc_stat         !< status flag of NetCDF routines

      REAL, DIMENSION(-1:nx+1,-1:ny+1,0:nt_ref) :: var_in   !< input array

      WRITE(*, '(A)') ' --- Read input file...'

      !- Open NetCDF file
      nc_stat = NF90_OPEN( input, NF90_NOWRITE, id_file )

      !- Get ID of array to read
      nc_stat = NF90_INQ_VARID( id_file, "phi", id_var_in )

      !- Read array
      nc_stat = NF90_GET_VAR( id_file, id_var_in, var_in,        &
                            start = (/ 1, 1, 1 /),               &
                            count = (/ nx+3, ny+3, nt_ref+1 /) )

      !- Resort input array and save to proper variable
      DO  t = 0, nt_ref
         DO  j = -1, ny+1
            DO  i = -1, nx+1
                phi_ref(i,j,t) = var_in(i,j,t)
            ENDDO
         ENDDO
      ENDDO

      WRITE(*, '(A)') '     ...finished'
      
   END SUBROUTINE read_input_data

   !----------------------------------------------------------------------------

   SUBROUTINE data_output(action, output)
   ! Data output to NetCDF file

      CHARACTER(LEN=*), INTENT(IN) :: action !< flag to steer routine (open/close/write)
      CHARACTER(LEN=*), INTENT(IN) :: output !< output file name prefix

      INTEGER, SAVE :: do_count=0      !< counting output
      INTEGER, SAVE :: id_dim_time     !< ID of dimension time
      INTEGER, SAVE :: id_dim_x        !< ID of dimension x
      INTEGER, SAVE :: id_dim_y        !< ID of dimension time
      INTEGER, SAVE :: id_file         !< ID of NetCDF file
      INTEGER, SAVE :: id_var_phi      !< ID of geopotential
      INTEGER, SAVE :: id_var_phiref   !< ID of reference geopotential
      INTEGER, SAVE :: id_var_time     !< ID of time
      INTEGER, SAVE :: id_var_u        !< ID of wind speed along x
      INTEGER, SAVE :: id_var_v        !< ID of wind speed along y
      INTEGER, SAVE :: id_var_x        !< ID of x
      INTEGER, SAVE :: id_var_y        !< ID of y
      INTEGER, SAVE :: id_var_zeta     !< ID of vorticity
      INTEGER, SAVE :: nc_stat         !< status flag of NetCDF routines

      INTEGER :: tt                    !< time index of ref. geopot.

      REAL, DIMENSION(:), ALLOCATABLE :: netcdf_data_1d  !< 1D output array

      REAL, DIMENSION(:,:), ALLOCATABLE :: netcdf_data_2d  !< 2D output array

      CALL system_clock (count, count_rate)           !< begin of time measurement
        diff_Ausgabe = REAL(count)/REAL(count_rate)

      SELECT CASE (TRIM(action))

         !- Initialize output file
         CASE ('open')

            !- Delete any pre-existing output file
            OPEN(20, FILE=TRIM(output)//'.nc')
            CLOSE(20, STATUS='DELETE')

            !- Open file
            nc_stat = NF90_CREATE(TRIM(output)//'.nc', NF90_NOCLOBBER, id_file)
            IF (nc_stat /= NF90_NOERR)  PRINT*, '+++ netcdf error'

            !- Write global attributes
            nc_stat = NF90_PUT_ATT(id_file, NF90_GLOBAL,  &
                                    'Conventions', 'COARDS')
            nc_stat = NF90_PUT_ATT(id_file, NF90_GLOBAL,  &
                                    'title', 'barotropic nwp-model')

            !- Define time coordinate
            nc_stat = NF90_DEF_DIM(id_file, 'time', NF90_UNLIMITED,  &
                                    id_dim_time)
            nc_stat = NF90_DEF_VAR(id_file, 'time', NF90_DOUBLE,  &
                                    id_dim_time, id_var_time)
            nc_stat = NF90_PUT_ATT(id_file, id_var_time, 'units',  &
                                    'seconds since 1900-1-1 00:00:00')

            !- Define spatial coordinates
            nc_stat = NF90_DEF_DIM(id_file, 'x', nx+1, id_dim_x)
            nc_stat = NF90_DEF_VAR(id_file, 'x', NF90_DOUBLE,  &
                                    id_dim_x, id_var_x)
            nc_stat = NF90_PUT_ATT(id_file, id_var_x, 'units', 'meters')

            nc_stat = NF90_DEF_DIM(id_file, 'y', ny+1, id_dim_y)
            nc_stat = NF90_DEF_VAR(id_file, 'y', NF90_DOUBLE,  &
                                    id_dim_y, id_var_y)
            nc_stat = NF90_PUT_ATT(id_file, id_var_y, 'units', 'meters')

            !- Define output arrays
            nc_stat = NF90_DEF_VAR(id_file, 'phi', NF90_DOUBLE,           &
                                    (/id_dim_x, id_dim_y, id_dim_time/),  &
                                    id_var_phi)
            nc_stat = NF90_PUT_ATT(id_file, id_var_phi,  &
                                    'long_name', 'geopotential at 500hPa')
            nc_stat = NF90_PUT_ATT(id_file, id_var_phi,  &
                                    'short_name', 'geopotential')
            nc_stat = NF90_PUT_ATT(id_file, id_var_phi, 'units', 'm2/s2')

            nc_stat = NF90_DEF_VAR(id_file, 'phi_ref', NF90_DOUBLE,       &
                                    (/id_dim_x, id_dim_y, id_dim_time/),  &
                                    id_var_phiref)
            nc_stat = NF90_PUT_ATT(id_file, id_var_phiref,  &
                                    'long_name',            &
                                    'reference geopotential at 500hPa')
            nc_stat = NF90_PUT_ATT(id_file, id_var_phiref,  &
                                    'short_name', 'ref. geopotential')
            nc_stat = NF90_PUT_ATT(id_file, id_var_phiref, 'units', 'm2/s2')

            nc_stat = NF90_DEF_VAR(id_file, 'zeta', NF90_DOUBLE,          &
                                    (/id_dim_x, id_dim_y, id_dim_time/),  &
                                    id_var_zeta)
            nc_stat = NF90_PUT_ATT(id_file, id_var_zeta,  &
                                    'long_name', 'vorticity at 500hPa')
            nc_stat = NF90_PUT_ATT(id_file, id_var_zeta,  &
                                    'short_name', 'vorticity')
            nc_stat = NF90_PUT_ATT(id_file, id_var_zeta, 'units', '1/s')

            nc_stat = NF90_DEF_VAR(id_file, 'u', NF90_DOUBLE,             &
                                    (/id_dim_x, id_dim_y, id_dim_time/),  &
                                    id_var_u)
            nc_stat = NF90_PUT_ATT(id_file, id_var_u,  &
                                    'long_name',       &
                                    'u component of wind')
            nc_stat = NF90_PUT_ATT(id_file, id_var_u, 'short_name', 'u')
            nc_stat = NF90_PUT_ATT(id_file, id_var_u, 'units', 'm/s')

            nc_stat = NF90_DEF_VAR(id_file, 'v', NF90_DOUBLE,             &
                                    (/id_dim_x, id_dim_y, id_dim_time/),  &
                                    id_var_v)
            nc_stat = NF90_PUT_ATT(id_file, id_var_v,  &
                                    'long_name',       &
                                    'v component of wind')
            nc_stat = NF90_PUT_ATT(id_file, id_var_v, 'short_name', 'v')
            nc_stat = NF90_PUT_ATT(id_file, id_var_v, 'units', 'm/s')


            !- Leave define mode
            nc_stat = NF90_ENDDEF(id_file)

            !- Write axis to file
            ALLOCATE(netcdf_data_1d(0:nx))

            !- x axis
            DO  i = 0, nx
               netcdf_data_1d(i) = i * delta
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_x, netcdf_data_1d,  &
                                    start = (/1/), count = (/nx+1/))
            !- y axis
            DO  j = 0, ny
               netcdf_data_1d(j) = j * delta
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_y, netcdf_data_1d,  &
                                    start = (/1/), count = (/ny+1/))

            DEALLOCATE(netcdf_data_1d)


         !- Close NetCDF file
         CASE ('close')

            nc_stat = NF90_CLOSE(id_file)


         !- Write data arrays to file
         CASE ('write')

            ALLOCATE(netcdf_data_2d(0:nx,0:ny))

            do_count = do_count + 1

            !- Write time
            nc_stat = NF90_PUT_VAR(id_file, id_var_time, (/t_sim/),  &
                                    start = (/do_count/),               &
                                    count = (/1/))

            !- Write geopotential
            DO  i = 0, nx
               DO  j = 0, ny
                  netcdf_data_2d(i,j) = phi(i,j)
               ENDDO
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_phi, netcdf_data_2d,  &
                                    start = (/1, 1, do_count/),          &
                                    count = (/nx+1, ny+1, 1/))

            !- Write reference geopotential
            tt = INT(ts/6)     ! time index of reference geopotential
            DO  i = 0, nx
               DO  j = 0, ny
                  netcdf_data_2d(i,j) = phi_ref(i,j,tt)
               ENDDO
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_phiref, netcdf_data_2d,  &
                                    start = (/1, 1, do_count/),             &
                                    count = (/nx+1, ny+1, 1/))

            !- Write vorticity
            DO  i = 0, nx
               DO  j = 0, ny
                  netcdf_data_2d(i,j) = zeta(i,j)
               ENDDO
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_zeta, netcdf_data_2d,  &
                                    start = (/1, 1, do_count/),           &
                                    count = (/nx+1, ny+1, 1/))
! 
            !- Write u
            DO  i = 0, nx
               DO  j = 0, ny
                  netcdf_data_2d(i,j) = u(i,j)
               ENDDO
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_u, netcdf_data_2d,  &
                                    start = (/1, 1, do_count/),        &
                                    count = (/nx+1, ny+1, 1/))

            !- Write v
            DO  i = 0, nx
               DO  j = 0, ny
                  netcdf_data_2d(i,j) = v(i,j)
               ENDDO
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_v, netcdf_data_2d,  &
                                    start = (/1, 1, do_count/),        &
                                    count = (/nx+1, ny+1, 1/))

            DEALLOCATE(netcdf_data_2d)


         !- Print error message if unknown action selected
         CASE DEFAULT

            WRITE(*, '(A)') ' ** data_output: action "'//  &
                              TRIM(action)//'"unknown!'

      END SELECT
      
      CALL system_clock (count, count_rate)           
        diff_Ausgabe = REAL(count)/REAL(count_rate) - diff_Ausgabe

   END SUBROUTINE data_output


END PROGRAM nwp_program
