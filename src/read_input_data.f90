   SUBROUTINE read_input_data(input)
   ! Read input from mesoscale model

      CHARACTER(LEN=*), INTENT(IN) :: input  !< name of input file

      INTEGER, SAVE :: id_file         !< ID of NetCDF file
      INTEGER, SAVE :: id_var_in       !< ID of input array
      INTEGER, SAVE :: nc_stat         !< status flag of NetCDF routines

      REAL, DIMENSION(-1:nx+1,-1:ny+1,0:nt_ref) :: var_in   !< input array

      WRITE(*, '(A)') ' --- Read input file...'

      !- Open NetCDF file
      nc_stat = NF90_OPEN( input, NF90_NOWRITE, id_file )

      !- Get ID of array to read
      nc_stat = NF90_INQ_VARID( id_file, "phi", id_var_in )

      !- Read array
      nc_stat = NF90_GET_VAR( id_file, id_var_in, var_in,        &
                            start = (/ 1, 1, 1 /),               &
                            count = (/ nx+3, ny+3, nt_ref+1 /) )

      !- Resort input array and save to proper variable
      DO  t = 0, nt_ref
         DO  i = -1, nx+1
            DO  j = -1, ny+1
               phi_ref(j,i,t) = var_in(i,j,t)
            ENDDO
         ENDDO
      ENDDO

      WRITE(*, '(A)') '     ...finished'

   END SUBROUTINE read_input_data
