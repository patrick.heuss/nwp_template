#!/usr/bin/python3
"""
Plotting script for programming exercises of lecture 'Programmierpraktikum zur
Numerischen Wettervorhersage'.

Tobias Gronemeier - 2019-02-04 (was 2018-03-08)
"""

import numpy as np

import matplotlib
import matplotlib.pyplot as plt

from netCDF4 import Dataset

import os

#------------------------------------------------------------------------------#

print("start plotting...")

#define plot
inputfile = "nwp_ue2_2_48h.nc"
outputfile = "ue2_2_48h.png"


#read data from output file
ncfile = Dataset(inputfile, mode='r')

x = ncfile.variables['x'][:] * 0.001
y = ncfile.variables['y'][:] * 0.001
time = ncfile.variables['time'][:] / 3600.0

phi = ncfile.variables['phi'][:,:]
phi_ref = ncfile.variables['phi_ref'][:,:]
zeta = ncfile.variables['zeta'][:,:]
u = ncfile.variables['u'][:,:]
v = ncfile.variables['v'][:,:]


#create plot
plt.title('Geopotential zeitliche Entwicklung 48h')
cmap = plt.get_cmap('bwr')
plt.grid(True)

plt.xlabel('x (km)')
plt.ylabel('y (km)')

#plt.subplot(2, 2)
plt.contourf(x, y, phi[114,:,:], cmap=cmap)

#plt.subplot(2, 2)
#plt.contour(x, y, phi_ref[0,:], cmap=cmap)

#plt.subplot(2, 2, 2)
#plt.contourf(x, y, zeta[0,:,:], cmap=cmap)

#plt.subplot(2, 2, 3)
#plt.contourf(x, y, u[0,:,:], cmap=cmap)

#plt.subplot(2, 2, 4)
#plt.contourf(x, y, v[0,:,:], cmap=cmap)


plt.savefig(outputfile)
plt.show()



print("finished plotting...")
